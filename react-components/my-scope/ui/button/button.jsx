import React from "react";
import styles from "./button.module.css";

export function Button({ text }) {
  return <button className={styles.button}>{text}</button>;
}
