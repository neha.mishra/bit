import React from 'react';
// import styles from"card.css";
import {Button} from "@nehamishra/my-components.ui.button";
import  style from './card.module.scss'

export type CardProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string,

    /**
   * a image to be rendered in the component.
   */
  imageUrl: string,
  /**
   * description
   */
  description: string,

/**
 *text for button 
  */
 buttonText: string;
};

export function Card({ text, buttonText="click me", imageUrl="https://images.unsplash.com/photo-1610146016991-d2089e51efaf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80"
, description="This is a card description" }: CardProps) {
  return (
    <div className={style.card}>
      {/* <img src="https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80"/> */}
    <img src={imageUrl}/>
     <h2  className={style.title}> {text}</h2>
     <h6  className={style.description}>{description}</h6>
      <Button text={buttonText} /> </div>
  );
}
