import React from 'react';
import { Card } from './card';

export const BasicCard = () => (
  <Card buttonText="Button" text="Chicago" imageUrl="https://images.unsplash.com/photo-1610146016991-d2089e51efaf?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80" description="Chicago skyline with fog at night"/>
);

export const CardWithDetails = () => (
  <Card buttonText="Show Details" text="Tokyo" imageUrl="https://images.unsplash.com/photo-1533050487297-09b450131914?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1350&q=80" description="Wandering through the small back alleys of Sangenjaya, Tokyo, around midnight, after the last train has left and only the locals are still around drinking and enjoying lively conversations. This place is packed to the brim with tiny bars that only fit a few people, shoulder to shoulder. Each bar with it’s own vibe and new experience to enjoy." />
);

export const Example = () => (
  <Card buttonText="Show Details" text="Times Square " imageUrl="https://images.unsplash.com/photo-1602940659805-770d1b3b9911?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80" description="Times Square in New York City at night." />
);
