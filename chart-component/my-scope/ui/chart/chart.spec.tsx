import React from 'react';
import { render } from '@testing-library/react';
import { BasicChart } from './chart.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicChart />);
  const rendered = getByText('hello from Chart');
  expect(rendered).toBeTruthy();
});
