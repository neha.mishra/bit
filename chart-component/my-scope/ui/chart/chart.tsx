import React from 'react';
import Plot from 'react-plotly.js'

export type ChartProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function Chart({ text }: ChartProps) {
  return (
    <div>
      <Plot
      data={[
        {
          x: /* xAxisArray */["1","2","3","4"],
          y: /* yAxisArray */[22,11,44,55]
        }
      ]}
      layout={{  title: /* title */"trial"}}
    />
      {text}
    </div>
  );
}
